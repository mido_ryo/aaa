﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed;

    //public GameObject Camera;

    private Rigidbody rb;

    //public int Hp;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        //CameraController cc = Camera.GetComponent<CameraController>();
        //cc.getTransform();
        //Camera.GetComponent<CameraController>().getTransform();
        //cc.getHP();
    }

    void FixedUpdate ()
    {
        // 入力をゲット.
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);
    }

    // ぶつかるとき.
    private void OnCollisionEnter(Collision collision)
    {
        
    }

    // ぶつかんないときめりこみ isTriggerがon ,otherはめりこんだ物体.
    private void OnTriggerEnter(Collider other)
    {
        // タグを作成！pick upタグなら.
        if(other.gameObject.CompareTag("pick up"))
        {
           //other.gameObject.GetComponent<pickController>().

            //if (Hp == 0)
            {             // そのオブジェクトを消す.
                other.gameObject.SetActive(false);
            }
        }
    }
}